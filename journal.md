# Journal d'activité

## 02/05
Prise en main du projet :
- Lecture de la Documentation
- Découverte et explication par Camille de la structure du code
- Découverte de ce que l'ancien stagiaire à réalisé via son rapport de stagiaire

Mise en place du poste de travail
- Mise en place d'un GitLab
- Mise en place d'un Element.io
- Installation de la machine virtuelle linux

## 03/05
Début du travail
- Fork du projet github sur GitLab (désactivationd des tests automatisé pour les push)
- Installation de pulsar-edit sur le poste de travail
- Configuration de la machine virtuelle
- Tentative d'installation de PyScada sous docker, problème d'accès a pip3 install
- Tentative d'installation de PyScada sur la VM, probleme, libhdf5-100 -> aucune version suceptible d'être installé (arret au niveau de l'installation de Django)

## 04/05
Installation de pyscada sur la VM
- Installation réussie
- Prise en note des différents problèmes
Installation impossible sur docker
- Grosse difficultés avec docker (problemes de proxy)

Prévision pour le 05/05
- Commenter le install.sh
- Mise a jour de la documentation
- tentative d'installation sur docker (probleme du proxy résolu lundi au plus tard, avec l'autre PC qui ne passera plus par ce réseau)

## 05/05
- Commentaire sur le install.sh

## 09/05
- Tentative d'installation de pyscada

## 10/05
- Tentative d'installation de pyscada

## 11/05
- Réussite de l'installation de pyscada

## 12/05
- Refactorisation du install_system.sh
- Ajout du logging du install_system.sh
- Réunion element avec Camille
- Ajout d'options pour le superuser django dans le install_system.sh