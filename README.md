# Etapes

## Installation
Installation et vérification de la doc (mise à jour si besoin)
- Docker
- System
- VEnv Python


## Documentation utilisateur / configurateur
Apres compréhension plus approfondie du logiciel, rédaction de la documentation manquante sur le readthedoc.io (lien de la documentation à intégrer quand le miens sera crée)


## Sauvegarde restauration
- System
    - Vérification du bon fonctionnement de la sauvegarde
    - Rédaction de la doc associée
- Docker
    - Trouver la procédure de sauvegarde simple pour les utilisateurs, en passant par un déploiement sous docker
    - Trouver la procédure de restauration
    - Rédaction de la doc associée

- Ajouter un bouton de sauvegarde

## Développement des widgets graphique
- Reprise et compréhension du code JavaScript existant
- Refactorisation si nécessaire
    - Utilisation de Sphynx pour générer la doc développeur associée
- Analyse des widgets Grafana
- Création de widgets similaires avec Flot ou UPlot (plus Uplot quand meme)
- Laisser le choix de l'affichage avec UPlot ou Flot

## Développement système
- Mettre en place un systeme push pour changer le fonctionnement actuel (requete tout les x seconde de chaque instance de pyscada vers le navigateur)
